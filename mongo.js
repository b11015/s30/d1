db.fruits.insertMany([
        {

                name: "Apple",
                supplier: "Red Farms Inc.",
                stocks: 20,
                price: 40,
                onSale: true
        },
        {

                name: "Banana",
                supplier: "Yellow Farms",
                stocks: 15,
                price: 20,
                onSale: true
        },
        {

                name: "Kiwi",
                supplier: "Green Farming and Canning",
                stocks: 25,
                price: 50,
                onSale: true
        },
        {

                name: "Mango",
                supplier: "Yellow Farms",
                stocks: 10,
                price: 60,
                onSale: true
        },
        {

                name: "Dragon Fruit",
                supplier: "Red Farms Inc.",
                stocks: 10,
                price: 60,
                onSale: true
        }
])

// Aggregation in MongoDB
        // process of generating manipulated data and perform operators to create filtered results that helps in data analysis
        // this helps in creating reports from analyzing data provided in our documents

        // Aggregations Pipelines
                // aggregation is done 2 to 3 steps
                // $match - checks the documents that will pass the condition
                        // Syntax: {$match: {existingField: value}}
                // $group - grouped documents together to create analysis of these grouped documents
                        // Syntax: {$group: {_id: <$id or exixstingField>, fieldResultName: "$valueResult"}}
                // $sum - get the total of all value in the given field

// aggregation is done 2 to 3 steps
// for stocks sum
db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}  
        }
])

// for price sum
db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "$supplier", totalPrice: {$sum: "$price"}}  
        }
])

// Mini - activity

// SOLUTION
db.fruits.aggregate([
        {
                $match: {supplier:  "Red Farms Inc."}
        },
        {
                $group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}  
        }
])

// average price of all items on sale ($avg)

db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "avgPriceOnSale", avgPrice: {$avg: "$price"}}
        }
])

// multiply operator

db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $project: {supplier: 1, _id: 0, inventory: {$multiply: ["$price", "$stocks"]}}
        }
])

// $max - highest value out of all the values in the given field

db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "maxStockOnSale", maxStock: {$max: "$stocks"}}
        }
])

// $min - lowest value out of all the values in the given field
db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "minStockOnSale", minStock: {$min: "$stocks"}}
        }
])

// $count - counting the items

db.fruits.aggregate([
        {
                $match: {onSale: true}    
        },
        {
                $count: "itemForSale"
        }
])

// $count - counting the items
db.fruits.aggregate([
        {
                $match: {price: {$lt: 50}}    
        },
        {
                $count: "priceLessThan50"
        }
])

// count total number of items per group $sum 1 only

db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "$supplier", noOfItems: {$sum: 1}}
        }
])

db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "$supplier", noOfItems: {$sum: 1}, totalStocks: {$sum: "$stocks"}}
        }
])

db.fruits.aggregate([
        {
                $match: {onSale: true}
        },
        {
                $group: {_id: "$supplier", totalStocks: {$sum: "$stocks"}}
        },
        {
                $out: "totalStocksPerSupplier"
        }
])